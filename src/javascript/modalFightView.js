import View from './view';
import fightMock from './mock/fight'

import { 
  closeComponent
} from './components'

class ModalFightView extends View {
  constructor() {
    super();
    this.createModal();
  }

  initModal() {
    const modal = document.getElementById("fightModal");
    const closeButton = document.getElementById("closefightModal");
    
    // When the user clicks on <span> (x), close the modal
    closeButton.onclick = function() {
      modal.style.display = "none";
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  createModal() {
    this.element = this.createElement({ tagName: 'div', className: 'modal-fight-content' });

    const contentElement = this.createElement({ tagName: 'div', className: '' })
    contentElement.innerHTML = fightMock;

    this.element.append(closeComponent('closefightModal'), contentElement);
  }
}

export default ModalFightView;