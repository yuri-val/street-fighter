import View from "../view";

const view = new View();


const times = (n, fn, context = undefined) => {
  let i = 0;
  while (fn.call(context, i) !== false && ++i < n) {}
};


export const closeComponent = (id) => {
    const spanClose = view.createElement({tagName: 'span', className: 'close', attributes: { id }});
    spanClose.innerText = "x";
    return spanClose;
}

export const headerComponent = () =>  {
  return view.createElement({tagName: 'h2', className: '', attributes: { id: 'fighterName'}});
}

export const inputIdComponent = () =>  {
  return view.createElement({tagName: 'input', className: '', attributes: {id: 'id', name: 'id', hidden: true}});
}

export const healthComponent = () =>  {
  const label = view.createElement({tagName: 'label', className: '', attributes: { for: 'health'}});
  const input = view.createElement({tagName: 'input', className: '', attributes: { id: 'health', name: 'health', type: 'range', min: 1, max: 100, step: 1}});
  const heart = view.createElement({tagName: 'i', className: 'fas fa-heart red-heart'});
  const healthRate = view.createElement({tagName: 'span', className: '', attributes: { id: 'healthRate'}});

  label.innerText = 'Health: ';

  const content = view.createElement({tagName: 'p', className: ''});

  content.append(label, input, heart,healthRate)

  return content;
}

export const attackComponent = () =>  {
  const label = view.createElement({tagName: 'label', className: '', attributes: { for: 'attack'}});
  const input = view.createElement({tagName: 'input', className: '', attributes: { id: 'attack', name: 'attack', hidden: true}});

  const icons = [];
  times(5, ind => icons.push(view.createElement({tagName: 'i', className: 'fas fa-hand-rock unchecked', attributes: { 'data-value': ind + 1 }})))

  label.innerText = 'Attack: ';

  const content = view.createElement({tagName: 'p', className: 'attack'});

  content.append(label, input, ...icons)

  return content;
}

export const defenseComponent = () =>  {
  const label = view.createElement({tagName: 'label', className: '', attributes: { for: 'defense'}});
  const input = view.createElement({tagName: 'input', className: '', attributes: {id: 'defense', name: 'defense', hidden: true}});

  const icons = [];
  times(5, ind => icons.push(view.createElement({tagName: 'i', className: 'fas fa-shield-alt unchecked', attributes: {'data-value': ind  + 1 }})))

  label.innerText = 'Defense: ';

  const content = view.createElement({tagName: 'p', className: 'defense'});

  content.append(label, input, ...icons)

  return content;
}

export const saveButtonComponent = () =>  {
  const row =  view.createElement({ tagName: 'div', className: '' });
  const button = view.createElement({ tagName: 'button', className: 'floated-right', attributes: { id: 'saveFighter'} });

  button.innerText = 'Save';
  row.append(button);
  return row;
}