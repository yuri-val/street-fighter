class Fighter {
  constructor(attributes) {
    this.attributes = Object.keys(attributes);
    this.attributes.forEach(key => {
      this[key] = attributes[key];
    });
  }

  getHitPower() {
    const criticalHitChance = Math.random() + 1;
    return this.attack * criticalHitChance;
  }

  getBlockPower() {
    const dodgeChance = Math.random() + 1;
    return this.defense * dodgeChance;
  }

  toJson() {
    const data = {}
    this.attributes.forEach(key => { data[key] = this[key] })
    return JSON.stringify(data);
  }
}

export default Fighter;