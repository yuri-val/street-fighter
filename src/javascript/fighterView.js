import View from './view';
import { fighterService } from './services/fightersService'
import { getItem } from './store'

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { id, name, source } = fighter;
    const nameElement = this.createName(id, name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  handleCheck(event, id, name) {
    if (!event.target.checked){
      fighterService.removeFromFight(id);
    } else {
      if (!fighterService.addToFight(name, id)) {
        event.target.checked = false;
      }
    }
    fighterService.updateFightView();
  }

  isFighterChecked(id) {
    const fighters = getItem('fighters');
    if (!fighters) return false;
    const { fighter_1, fighter_2 } = fighters;
    return fighter_1 && fighter_1.id === id || fighter_2 && fighter_2.id == id; 
  }

  createName(id, name) {
    const checkbox = this.createElement({ tagName: 'input', className: '', attributes: { type: 'checkbox', 'data-id': id, name: `fighter_${id}` }});
    const label = this.createElement({ tagName: 'label', className: '', attributes: { for: `fighter_${id}`}});
    label.innerText = name;

    let checked = this.isFighterChecked(id);

    checkbox.onchange = (event) => this.handleCheck(event, id, name);
    checkbox.checked = checked;
    
    const nameElement = this.createElement({ tagName: 'div', className: 'name' });
    nameElement.append(checkbox);
    nameElement.append(label);

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;