import View from './view';
import FighterView from './fighterView';
import Fighter from './models/fighter'
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    window.fightersDetailsMap = new Map();
    fighterService.updateFightView();
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    if (event.target.type === "checkbox") {
      return
    }
    const { id } = fighter;
    let fighterModel = window.fightersDetailsMap.get(id);

    if(!fighterModel) {
      const fighterDetails = await fighterService.getFighterDetails(id);
      fighterModel = new Fighter(fighterDetails);
      window.fightersDetailsMap.set(id, fighterModel);
    }

    this.showFighterDetailsModal(fighterModel);
  }

  showFighterDetailsModal(fighter) {
    const modal = document.getElementById("fighterDetails");
    document.getElementById("fighterName").innerText = fighter.name;
    document.getElementById("id").value = fighter.id;

    ['health', 'attack', 'defense'].forEach(key => {
      const element = document.getElementById(key);
      if (element) {
        element.value = fighter[key];
        if (key !== 'health') this.setChecked(document.querySelectorAll(`p.${key} i`), fighter[key])
        if (key === 'health') document.getElementById("healthRate").innerText = fighter[key];
      }
    })

    modal.style.display = "block"
  }

}

export default FightersView;