import Fighter from "./models/fighter";

class View {
  element;

  createElement({ tagName, className = '', attributes = {} }) {
    const element = document.createElement(tagName);
    if (className) element.classList.add(...className.split(' '));
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }

  setChecked(items, value) {
    items.forEach( el => { el.classList.add('unchecked') })
    items.forEach( (el, index) => { if(index < value) el.classList.remove('unchecked') })
  }

}

export default View;
