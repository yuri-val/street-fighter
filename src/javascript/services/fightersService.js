import { callApi } from '../helpers/apiHelper';
import { getItem, setItem } from '../store'
import View from '../view';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters';
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endpoint = `fighters/${id}`;
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async updateFighterDetails(id, data) {
    try {
      const endpoint = `fighters/${id}`;
      const apiResult = await callApi(endpoint, 'PUT', data);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
  
  updateFightView(){
    const viewHelper = new View();
    const { fighter_1, fighter_2 } = getItem('fighters') || {};
    if(fighter_1) {
      document.getElementById("fighter_1").innerText = fighter_1.name;
    }
    if(fighter_2) {
      document.getElementById("fighter_2").innerText = fighter_2.name;
    }
    let fightButton = document.getElementById("startFight");
    if (!fightButton) {
      fightButton = viewHelper.createElement({ tagName: 'button', className: 'fight-button', attributes: { id: 'startFight' }});
      fightButton.onclick = () => this.startFight(fighter_1, fighter_2);
      fightButton.innerText = 'Fight!'
      const el = document.querySelector('div.fighters-header');
      if (el) el.append(fightButton);
    }
    fightButton.style.display = fighter_1 && !!fighter_1.id && fighter_2 && !!fighter_2.id ? 'block' : 'none';
  }  

  addToFight(name, id) {
    let fighters = getItem('fighters');
    if (!fighters) {
      fighters = { fighter_1: { name: '', id: '' }, fighter_2: { name: '', id: '' } }
    }
    const { fighter_1, fighter_2 } = fighters;
    if (!!fighter_1.id && !!fighter_2.id) return false;
    if (!fighter_1.id) {
      fighters.fighter_1 = { name, id };
    } else {
      fighters.fighter_2 = { name, id };
    }
    setItem('fighters', fighters)
    return true;
  }

  removeFromFight(id) {
    let fighters = getItem('fighters');
    if (!fighters) {
      fighters = { fighter_1: { name: '', id: '' }, fighter_2: { name: '', id: '' } }
    }
    const { fighter_1, fighter_2 } = fighters;
    if (fighter_1 && fighter_1.id === id) fighters.fighter_1 = { name: '', id: '' };
    if (fighter_2 && fighter_2.id === id) fighters.fighter_2 = { name: '', id: '' };
    setItem('fighters', fighters)
  }

  startFight(fighter_1, fighter_2) {
    const fightModal = document.getElementById('fightModal')

    fightModal.style.display = 'block';
  }

}

export const fighterService = new FighterService();
