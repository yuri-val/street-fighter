import FightersView from './fightersView';
import ModalView from './modalView';
import ModalFightView from './modalFightView';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');
  static modalFormElement = document.getElementById('fighterDetails');
  static modalFightFormElement = document.getElementById('fightModal');
    

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);

      const modalForm = new ModalView();
      const modalElement = modalForm.element;
      App.modalFormElement.appendChild(modalElement);
      modalForm.initModal();

      const modalFightForm = new ModalFightView();
      const modalFightElement = modalFightForm.element;
      App.modalFightFormElement.appendChild(modalFightElement);
      modalFightForm.initModal();

      
      
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;