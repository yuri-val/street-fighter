import View from './view';
import { fighterService } from './services/fightersService';

import { 
  closeComponent, 
  headerComponent,  
  inputIdComponent,
  healthComponent,
  attackComponent,
  defenseComponent,
  saveButtonComponent
} from './components'

class ModalView extends View {
  constructor() {
    super();
    this.createModal();
  }

  initModal() {
    const modal = document.getElementById("fighterDetails");
    const closeButton = document.getElementById("closeModal");
    const health = document.getElementById("health");
    const saveFighter = document.getElementById("saveFighter");
    
    // When the user clicks on <span> (x), close the modal
    closeButton.onclick = function() {
      modal.style.display = "none";
    };

    health.oninput = function() {
      document.getElementById("healthRate").innerText = health.value;
    };

    saveFighter.onclick = function() {
      const _id = document.getElementById("id").value;
      const fighter = window.fightersDetailsMap.get(parseInt(_id));
      ['health', 'attack', 'defense'].forEach(key => { fighter[key] = document.getElementById(key).value });
      fighterService.updateFighterDetails(_id, fighter.toJson())
        .then(() => modal.style.display = "none")
      
    };

    ['attack', 'defense'].forEach(query => {
      const items = document.querySelectorAll(`p.${query} i`);
      items.forEach((shield) => {
        shield.onclick = () => {
          document.getElementById(query).value = shield.dataset.value;
          this.setChecked(items, shield.dataset.value)
        }
      })
    });

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  createModal() {
    this.element = this.createElement({ tagName: 'div', className: 'modal-content' });

    this.element.append(closeComponent('closeModal'), headerComponent(), inputIdComponent(), 
                        healthComponent(), attackComponent(), defenseComponent(),
                        saveButtonComponent() );
  }
}

export default ModalView;