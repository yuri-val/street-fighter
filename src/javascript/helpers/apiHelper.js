const API_URL =  process.env.NODE_ENV === 'production' ?
  'https://radiant-tundra-60765.herokuapp.com/' :
  'http://localhost:3000/'

function callApi(endpoind, method, body) {
  const url = API_URL + endpoind;
  const options = {
    method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body
  };
  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi }