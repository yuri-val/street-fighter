export default `

<div class="grid">
<div class="row row-header">
  <div class="header">
    <div class="pull-center">
      Ryu 
      <div>
        <progress value="45" max="100" />  
      </div>
    </div>
  </div>
  <div class="header-center">
    <div class="pull-center">
      vs
    </div>
  </div>
  <div class="header">
    <div class="pull-center">
      Ken 
      <div>
        <progress value="77" max="100" />  
      </div>
    </div>
  </div>
</div>
<div class="row row-content">
  <div class="col-content">
    <img class="fighter-image" src="http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif">  
  </div>
  <div class="col-content">
    <img class="fighter-image img-flip" src="http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif">  
  </div>
  
</div>
<div class="row">
  <div class="pull-center">
    <h1>It's just a Mock. Sorry I did not have a time.</h1>
  </div>            
</div>
</div>

`