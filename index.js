import App from './src/javascript/app';
import './src/styles/styles.css';
import './src/styles/modal.css';
import './src/styles/fight.css';

require('file-loader?name=[name].[ext]!./index.html');
require('file-loader?name=[name].[ext]!./resources/logo.png');

new App();