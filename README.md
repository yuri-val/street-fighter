# stage-2-es6-for-everyone

## Demo

You can find last `master`'s demo here https://kind-goldstine-ce3d02.netlify.com/

Backend: https://radiant-tundra-60765.herokuapp.com/

## Run (Development)

As we are using external API, you need to start server before.

1. Clone next repo:

`git clone https://yuri-val@bitbucket.org/yuri-val/street-fighter-backend.git`

2. Install dependencies:

`npm install`

3. Run server (Express.js)

`npm run start`

Server will be avaliable at http://localhost:3000

4. Run this project 

`npm install`

`npm run start`

open http://localhost:8080/

